#!/usr/bin/env bash

# SETUP --------------
GITLAB_PROJECT="minutario/"
DOKKU_HOST="minutario.xyz"
DJANGO_DEFAULT_SITE_NAME="Minutario"
DEFAULT_FROM_EMAIL="Minutario <noreply@minutario.online>"
SPARKPOST_TOKEN="replace-me"

# NOTE: SENTRY_TOKEN should have permissions: project:read and project:write
# Get SENTRY_TOKEN here: https://sentry.io/settings/account/api/auth-tokens/
SENTRY_TOKEN="replace-me"
SENTRY_ORGANIZATION_SLUG="nan-systems"
SENTRY_TEAM_SLUG="minutario" # minutario, nan-systems, aos-fatos, kolab or videohub
# --------------------

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m"

check_status() {
  if [ "$?" != 0 ]; then
    echo -e "$RED[ error ]$NC"
    return -1
  fi
  echo -e "$GREEN[ done ]$NC"
}

setup_remote_git() {
  echo "* Setting up remote git..."
  git remote add origin "git@gitlab.com:nan-systems/$GITLAB_PROJECT$(git rev-parse --show-toplevel | xargs basename).git" &&
  git push -u --all origin &&
  check_status
}

create_dokku_app() {
  echo "* Creating dokku app ..."
  DOKKU_HOST="$DOKKU_HOST" dokku apps:create {{ project_name }} &&
  check_status
}

setup_sentry() {
  # Sentry API Reference: https://docs.sentry.io/api/teams/create-a-new-project/
  # Sentry API Reference: https://docs.sentry.io/api/projects/list-a-projects-client-keys/

  echo "Setting up Sentry..."
  curl -s "https://sentry.io/api/0/teams/$SENTRY_ORGANIZATION_SLUG/$SENTRY_TEAM_SLUG/projects/" \
       -H "Authorization: Bearer $SENTRY_TOKEN" \
       -H 'Content-Type: application/json' \
       -d '{"name":"{{ project_name }}","slug":"{{ project_name }}","platform":"python-django"}' &&

  SENTRY_DSN=$(curl -s "https://sentry.io/api/0/projects/$SENTRY_ORGANIZATION_SLUG/{{ project_name }}/keys/" \
                    -H "Authorization: Bearer $SENTRY_TOKEN" | tac | tac | python3 -c "import sys, json; data=json.loads(sys.stdin.readline()); print(data[0]['dsn']['secret'])") &&
  dokku config:set SENTRY_DSN="$SENTRY_DSN" &&
  check_status
}

setup_remote_postgres() {
  echo "* Setting up postgres..."
  dokku postgres:create {{ project_name }} &&
  dokku postgres:link {{ project_name }} {{ project_name }}
  check_status
}

setup_dukku_config() {
  echo "* Setting up ALLOWED_HOSTS, SECRET_KEY, DEFAULT_FROM_EMAIL, and SPARKPOST_TOKEN..."
  dokku config:set SPARKPOST_API_KEY="$SPARKPOST_TOKEN" \
      ALLOWED_HOSTS="{{ project_name }}.$DOKKU_HOST" \
      SECRET_KEY="$(pipenv run python manage.py generate_secret_key)" \
      $(echo "DEFAULT_FROM_EMAIL='$DEFAULT_FROM_EMAIL'") \
      --no-restart
}

setup_dokku() {
  setup_dukku_config &&

  git push dokku HEAD:master &&

  dokku run python manage.py createsuperuser &&
  dokku run python manage.py set_default_site --name "$DJANGO_DEFAULT_SITE_NAME" --domain "{{ project_name }}.$DOKKU_HOST" &&

  dokku letsencrypt:enable &&
  check_status
}

print_help() {
    echo "Usage: $(basename $0) [OPTIONS]

OPTIONS:
    -a, --all
    -g, --setup_remote_git
    -c, --create_dokku_app
    -p, --setup_remote_postgres
    -s, --setup-sentry
    -d, --setup_dokku
    -h, --help"
}


while [ -n "$1" ]
do
    case $1 in
        -g | --setup_remote_git) setup_remote_git ;;
        -s | --setup_sentry) setup_sentry ;;
        -c | --create_dokku_app) create_dokku_app ;;
        -p | --setup_remote_postgres) setup_remote_postgres;;
        -d | --setup_dokku) setup_dokku ;;
        -a | --all)
             setup_remote_git &&
             create_dokku_app &&
             setup_remote_postgres &&
             setup_sentry &&
             setup_dokku
            ;;
        -h | --help) print_help;;
        *) echo "Invalid option: $1" && print_help
    esac
    shift
done
