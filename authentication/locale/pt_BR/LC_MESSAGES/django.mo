��          �      l      �     �  0   �     ,     B  5   V     �  +   �     �  
   �     �     �          !     =  !   F     h     q     �  .   �     �  A  �     (  8   E     ~     �  B   �     �  7   �     7     >     M     \  '   |     �     �  &   �  	   �  '        )  :   0  5   k     	                                                                     
                        Account activation failed Account activation has failed. Please try again. Account activation on Activate account at Email with password reset instructions has been sent. Forgot password Link is valid for %(expiration_days)s days. Log in Not member Password changed Password reset failed Password reset on %(site_name)s Password reset successfully Register Registration is currently closed. Reset it Reset password at %(site_name)s Submit You are now registered. Activation email sent. Your account is now activated. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 A ativação da conta falhou A ativação da conta falhou. Tente novamente por favor. Ativação de conta no Ative sua conta em O e-mail com as instruções de redefinição de senha foi enviado Esqueci minha senha O link é válido somente por %(expiration_days)s dias. Log in Não tem conta Senha alterada A redefinição de senha falhou Redefinição de senha em %(site_name)s Senha redefinida com sucesso Crie sua conta O registro está fechado por enquanto. Redefinir Redefinição de senha em %(site_name)s Enviar Sua conta foi criada e o e-mail de ativação foi enviado. Sua conta foi ativada e você já pode fazer o login. 