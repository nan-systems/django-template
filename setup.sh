#!/usr/bin/env bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m"

check_status() {
  if [ "$?" != 0 ]; then
    echo -e "$RED[ error ]$NC"
    return -1
  fi
  echo -e "$GREEN[ done ]$NC"
}

create_db() {
  echo "* Creating local db..."
  createdb {{ project_name }}
  check_status
}

copy_env() {
  echo "* Copying environment variables..."
  cp env.example .env
  check_status
}

install_dev() {
  echo "* Running pipenv..."
  pipenv install --dev
  check_status
}

apply_migrations() {
  echo "* Applying migrations..."
  pipenv run python manage.py migrate
  check_status
}

create_superuser() {
  echo "* Creating admin superuser..."
  pipenv run python manage.py createsuperuser --username admin --email admin@example.com
  check_status
}

setup_repository() {
  echo "* Setting up local repository..."
  git flow init -d &&
  rm setup.sh &&
  git add . &&
  git restore --staged remote-setup.sh &&
  git commit -m 'Init project'
  check_status
}


print_help() {
    echo "Usage: $(basename $0) [OPTIONS]

OPTIONS:
    -a, --all
    -d, --create_db
    -e, --copy_env
    -i, --install_dev
    -m, --apply_migrations
    -s, --create_superuser
    -r, --setup_repository
    -a, --all
    -h, --help"
}


while [ -n "$1" ]
do
    case $1 in
        -d | --create_db) create_db ;;
        -e | --copy_env) copy_env ;;
        -i | --install_dev) install_dev ;;
        -m | --apply_migrations) apply_migrations ;;
        -s | --create_superuser) create_superuser ;;
        -r | --setup_repository) setup_repository ;;
        -a | --all)
            create_db &&
            copy_env &&
            install_dev &&
            apply_migrations &&
            create_superuser &&
            setup_repository
            ;;
        -h | --help) print_help;;
        *) echo "Invalid option: $1" && print_help
    esac
    shift
done
