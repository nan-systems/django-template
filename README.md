# Setup dev environment

```bash
$ django-admin startproject --template=django-template --name=Procfile,env.example,pytest.ini,setup.sh,remote-setup.sh,.gitlab-ci.yml,.tool-versions project_name
$ cd project_name
$ ./setup.sh
```

After entering the project folder, make sure that the postgres instace used is up:

```bash
$ pg_ctl start
```

# Remote setup

Update the variable values at the begening of the `remote-setup.sh` file, then run it using `source` in order to access the `dokku` function:

```bash
$ source remote-setup.sh -a
```
