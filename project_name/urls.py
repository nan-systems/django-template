from django.contrib import admin
from django.urls import path, include

from django_registration.backends.activation.views import RegistrationView

from authentication.forms import CustomUserForm

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "accounts/register/",
        RegistrationView.as_view(form_class=CustomUserForm),
        name="django_registration_register",
    ),
    path("accounts/", include("django_registration.backends.activation.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
]
